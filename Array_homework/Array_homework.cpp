﻿//08.08.2022
#include <iostream>
#include <time.h>

using namespace std;

const int STR = 5;//кол-во строк массива
const int COL = 5;//кол-во колонок массива

int main()
{
    struct tm buf;         //
    time_t t = time(NULL); //           Возвращает
    localtime_s(&buf, &t); //          сегодняшнюю
    buf.tm_mday;           //              дату

    int index = buf.tm_mday % COL; //индекс остатка деления текущего числа календаря (buf.tm_mday) на N (COL=5)

    int arr[STR][COL]{}, sum = 0;// {} заполнение массива нулями,контейнер суммы массива sum

    for (int i = 0; i < STR; i++)
    {
        sum = 0;

        for (int j = 0; j < COL; j++)
        {
            arr[i][j] = i + j;//заполнение массива элементами

            cout << arr[i][j] << " ";

            sum += arr[index][j];//сумма элементов определенной строки массива  
        }
        cout << endl << endl;
    }

    cout << endl << "On " << buf.tm_mday << " date summa = " << sum << endl;

    return 0;
}